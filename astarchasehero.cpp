//Yusra Rahman: yrahman1                                                       
//Chanha Kim: ckim135                                                          
//Robert Aviles: raviles2

#include "entitycontroller.h"
#include "position.h"
#include <vector>
#include "gamerules.h"
#include "game.h"
#include "entity.h"
#include "ecfactory.h"
#include "astarchasehero.h"
#include <iostream>
#include <array>
#include <map>
#include "maze.h"
#include <utility>
#include <map>

struct Node
{
  int x_pos;
  int y_pos;
  int x_parent;
  int y_parent;
  int gscore;
  int h_n;
  int fscore;
  int in_closed;
};

Direction AStarChaseHero::getMoveDirection(Game *game, Entity *entity){
    Position min_pos = entity->getPosition();//store  position of minatour
    std::vector<Entity *> hero_list = game->getEntitiesWithProperty('h');
    int min_distance = 500; //number to be overwritten
    Position hero_pos;
    GameRules * rul = game->getGameRules();
    for (std::vector<Entity *>::iterator it = hero_list.begin(); it != hero_list.end(); ++it){
      Position cur_hero =(*it)->getPosition(); //get position of the hero
      int distance = min_pos.distanceFrom(hero_pos);
      if (distance < min_distance) {
	min_distance = distance;
	hero_pos = cur_hero;
      }
    }
    int x_max = game->getMaze()->getWidth();
    int y_max  = game->getMaze()->getHeight();
    std::map<std::pair<int,int>,Node*> maze_pos;
    std::pair <int,int> cords;
    for (int  x= 0; x<x_max;x++){
      for (int  y=0;y<y_max;y++){
	Position c_node = Position(x,y);
	cords = std::make_pair(x,y);
	Node *temp=new Node;
	temp->x_pos= x;
	temp->y_pos =y;
	temp->x_parent = -1;
	temp->y_parent =-1;
	temp->gscore = 500000;
	temp->fscore = 500000;
	temp->h_n = c_node.distanceFrom(hero_pos);
	temp->in_closed = 0;
	maze_pos.insert(std::pair<std::pair<int,int>,Node*>(cords,temp));
			//maze_pos[cords]->y_pos=y;
			//maze_pos[cords]->x_parent = -1;
			//maze_pos[cords]->y_parent = -1;
			//maze_pos[cords]->gscore = 500000;
			//maze_pos[cords]->fscore = 500000;
			//maze_pos[cords]->h_n = c_node.distanceFrom(hero_pos);
			//maze_pos[cords]->in_closed = 0;
      }
    }
    std::cout << maze_pos[std::make_pair(3,4)]->x_parent << std::endl;
    cords = std::make_pair(min_pos.getX(),min_pos.getY());
    maze_pos[cords]->h_n = min_pos.distanceFrom(hero_pos);
    maze_pos[cords]->gscore = 0;
    maze_pos[cords]->fscore = min_pos.distanceFrom(hero_pos);
    maze_pos[cords]->x_parent = min_pos.getX();
    maze_pos[cords]->y_parent = min_pos.getY();
 
    std::map<std::pair<int,int>,Node*> openSet;
    openSet.insert(std::pair<std::pair<int,int>,Node*>(cords,maze_pos[cords]));
    while (!openSet.empty()){
      Node* element;
      int fscore_test  = 500000;
      std::map<std::pair<int,int>,Node*>::iterator it_num;
      for (std::map<std::pair<int,int>,Node*>::iterator it = openSet.begin(); it !=openSet.end();it++){//look for cheapest path in openSet
	Node* cur = it->second;
	if  (cur->fscore < fscore_test){
	  element = it->second;
	  fscore_test = element->fscore;
	  it_num = it;
	}
      }
      openSet.erase(it_num);
      maze_pos[std::make_pair(element->x_pos,element->y_pos)]->in_closed = 1;
      int  x_loc  = element->x_pos;
      int  y_loc = element->y_pos;
      Position elm_loc = Position(x_loc,y_loc);
      for (int i = -1; i<=1; i++){
	for (int j =-1; j<=1; j++){
	  Position loc = Position(x_loc+i,y_loc+j);
	  if (rul->allowMove(game,entity,elm_loc,loc)){
	    if ((j==0 && i ==0)){}
	    else{
	      if(loc == hero_pos){
		  //we found it get path
		int xn = elm_loc.getX();
	        int yn = elm_loc.getY();
		int xlast;
		int ylast;
		//std::cout << xn << "," << yn << std::endl;
		//std::cout << maze_pos[std::make_pair(3,4)]->x_parent << std::endl;

		while (maze_pos[std::make_pair(xn,yn)]->x_parent != xn && maze_pos[std::make_pair(xn,yn)]->y_parent !=yn){
		  xlast = xn;
		  ylast =yn;
		  xn = maze_pos[std::make_pair(xn,yn)]->x_parent;
		  yn = maze_pos[std::make_pair(xlast,yn)]->y_parent;
	        }
		int  x_pos = min_pos.getX();
		int  y_pos = min_pos.getY();
		std::cout <<xlast << ","<<ylast;
		if (x_pos >xlast){
		  return Direction::LEFT;}
		if (y_pos >ylast){
		  return Direction::DOWN;}
		if (x_pos <xlast){
		  return Direction::RIGHT;}
		if (y_pos <ylast){
		  return Direction::UP;}
	      }
	      else if (maze_pos[std::make_pair(x_loc+i,y_loc+j)]->in_closed == 0){
		int fupdate = elm_loc.distanceFrom(loc) + loc.distanceFrom(hero_pos) +element->gscore;
		if (fupdate <maze_pos[std::make_pair(x_loc+i,y_loc+j)]->fscore){
		  std::pair <int,int> cords2 = std::make_pair(x_loc+i,y_loc+j);
		  maze_pos[cords2]->fscore = fupdate;
		  maze_pos[cords2]->gscore = elm_loc.distanceFrom(loc) + element->gscore;
		  maze_pos[cords2]->h_n = loc.distanceFrom(hero_pos);
		  maze_pos[cords2]->x_parent = x_loc;
		  maze_pos[cords2]->y_parent = y_loc;
		  openSet.insert(std::pair<std::pair<int,int>,Node*>(cords2,maze_pos[cords2]));
		}
	      }
	    }
	  }
	}
      }
    }
	     return Direction::NONE;
	     }	     
	   
	   
bool AStarChaseHero::isUser() const {
  return false;
}


AStarChaseHero::AStarChaseHero() {
}

AStarChaseHero::~AStarChaseHero() {
}

