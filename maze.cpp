//Yusra Rahman: yrahman1                                                       
//Chanha Kim: ckim135                                                          
//Robert Aviles: raviles2 
#include "maze.h"
#include <string>
#include <sstream>
#include "position.h"
#include "tilefactory.h"
#include "tile.h"
#include "entity.h"

void Maze::clearTiles(std::vector<Tile*> &vec) {
  for (size_t i = 0; i < vec.size(); i++) {
    delete vec[i];
  }
}

Maze::Maze(int width, int height, std::vector<Tile*> grid){
  this->width = width;
  this->height = height;
  this->grid = grid;
}

Maze::Maze(int width, int height) {
  this->width = width;
  this->height = height;
  for (int i = 0; i < width * height; i++) {
    grid.push_back(nullptr);
  }
}

Maze::~Maze(){
  for (int i = 0; i < width * height; i++) {
    delete grid[i];
  }
}

int Maze::getWidth() const {
  return width;
}

int Maze::getHeight() const {
  return height;
}

bool Maze::inBounds(const Position &pos) const {
  int posx = pos.getX();
  int posy = pos.getY();
  return posx < width && posx >= 0 && posy < height && posy >= 0;
}

void Maze::setTile(const Position &pos, Tile *tile) {
  //delete grid[pos.getY() * width + pos.getX()];
  grid[pos.getY() * width + pos.getX()] = tile; 
}

const Tile * Maze::getTile(const Position &pos) const {
  return grid[pos.getY() * width + pos.getX()];
}

Maze * Maze::read(std::istream &in) {

  int new_width = 0;
  int new_height = 0;
  Tile * new_tile;
  std::vector<Tile*> new_grid;

  
  //Maze * new_maze = new Maze();
  std::string firstline;
  if (!std::getline(in, firstline)) {
    return nullptr; //not successful in reading in the first line
  } else {
    std::stringstream ss(firstline); //put firstline into stringstream
    ss >> new_width;
    ss >> new_height;
    if (new_width == 0 || new_height == 0) {
      return nullptr;
    }
  }


  std::string temp;
  for (int i = 0; i < new_height; i++) {
    std::getline(in, temp);
    if ((int)temp.length() != new_width) { //invalid number of columns
      clearTiles(new_grid);
      return nullptr;
    }
    for (int j = 0; j < new_width; j++) {
      char letter = temp[j];
      if (letter == ' ') {
	continue;
      }
      //check if letter is valid tile char
      if (!(letter == '#' || letter == '.' || letter == '<')) {
	clearTiles(new_grid);
	return nullptr;
      }
      TileFactory * ptr = TileFactory::getInstance();
      new_tile = ptr->createFromChar(letter);
      new_grid.push_back(new_tile);
    }
  }

  

  Maze * new_maze = new Maze(new_width, new_height, new_grid);

  return new_maze;
}



