#ifndef CHASEHERO_H
#define CHASEHERO_H

#include "entitycontroller.h"

class ChaseHero : public EntityController {
private:
  // disallow copy constructor and assignment operator
  ChaseHero(const ChaseHero &);
  ChaseHero &operator=(const ChaseHero &);

public:
  ChaseHero();
  virtual ~ChaseHero();

  virtual Direction getMoveDirection(Game *game, Entity *entity);
  virtual bool isUser() const;

private:
  // Add your own private member functions...

  //Checks if the passed in direction is possible for minatour
  //min_pos indicates the current position of controlled entity
  //return false if move is blocked
  //true otherwise
  bool checkHypothetical(Direction dir, Entity * minotour, Position &min_pos, Game *game);
  Direction checkVertical(Position &min_pos, Entity * entity, Position &entity_pos, Game *game);
  Direction checkHorizontal(Position &min_pos, Entity * entity, Position &entity_pos, Game *game);
  bool makeFaster(Direction dir, Position &min_pos, Position &hero_pos, int dx, int dy);
};

#endif // CHASEHERO_H
