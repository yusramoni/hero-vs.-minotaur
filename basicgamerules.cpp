//Yusra Rahman: yrahman1                                                      
//Chanha Kim: ckim135                                                          
//Robert Aviles: raviles2 
#include "basicgamerules.h"
#include "entity.h"
#include "position.h"
#include "maze.h"
#include "tile.h"
#include <vector>
#include "ui.h"

void BasicGameRules::display(Game * game, Entity * actor) const {
  if (actor->hasProperty('h')) {
    game->getUI()->displayMessage("Illegal Move", false);
  }
}

BasicGameRules::BasicGameRules(){
}

BasicGameRules::~BasicGameRules(){
}

bool BasicGameRules::allowMove(Game * game, Entity * actor, const Position &source, const Position &dest) const {
  
  //if the positions are not adjacent return false
  if (!(near(source, dest))) {
    display(game, actor);
    return false;
  }
  //if the source position is out of bounds return false
  if (!inBounds(game, source)){
    display(game, actor);
    return false;
  }
  //if the dest position is out of bounds return false
  if (!inBounds(game, dest)){
    display(game, actor);
    return false;
  }

  Direction d = getDirection(source, dest); //get direction of the movement
  const Tile * destTile = game->getMaze()->getTile(dest); //destination tile
  Entity * destEntity = game->getEntityAt(dest); //destination entity if it exists

  if (destTile->checkMoveOnto(actor, source, dest) != MoveResult::ALLOW) {//runs into a wall
    display(game, actor);
    return false; //destination tile doesn't allow movement
  }
  
  if (destEntity == nullptr) { //dest tile is empty
    return true;
  } else if (destEntity->hasProperty('v') == true) { //desttile occpuied by moveable entity
    if (actor->hasProperty('m')) { //minotaur return false
      return false;
    }
    Position destdest = dest.displace(d); //position of the destination of the destination
    if (!(inBounds(game, destdest))) { //if destdest out of bounds return false
      display(game, actor);
      return false;
    }
    const Tile * destdestTile = game->getMaze()->getTile(destdest);
    Entity * destdestEntity = game->getEntityAt(destdest);
    if (destdestTile->checkMoveOnto(actor, dest, destdest) != MoveResult::ALLOW) {
      display(game, actor);
      return false; //moveable entity cannot move
    }
    if (destdestEntity != nullptr) { //another entity present!
      display(game, actor);
      return false;
    }
    destEntity->setPosition(destdest);
    return true; //empty and moveable tile for the movable entity
  } else if (destEntity->hasProperty('h') == true || destEntity->hasProperty('m') == true) {
    return true;
  }
  display(game, actor);
  return false; //dest tile occupied by immovable entity
}    

void BasicGameRules::enactMove(Game * game, Entity * actor, const Position &dest) const {
  Position actorPosition = actor->getPosition();
  Direction d = getDirection(actorPosition, dest);
  Entity * destEntity = game->getEntityAt(dest);
  if (destEntity == nullptr){ //dest tile empty
    actor->setPosition(dest);
  } else { //dest tile not empty
    actor->setPosition(dest);
    if (destEntity->hasProperty('v')) {  
      Position destdest = dest.displace(d);
      destEntity->setPosition(destdest);
    }
  }
}

GameResult BasicGameRules::checkGameResult(Game * game) const {
  std::vector<Entity *> heroes = game->getEntitiesWithProperty('h');
  std::vector<Entity *> minos = game->getEntitiesWithProperty('m');
  Maze * maze = game->getMaze();
  for (size_t i = 0; i < heroes.size(); i++) {
    Position pos = heroes[i]->getPosition();
    if (maze->getTile(pos)->isGoal()) {
      return GameResult::HERO_WINS;
    }
  }
  for (size_t i = 0; i < minos.size(); i++) {
    Position m_pos = minos[i]->getPosition();
    for (size_t j = 0; j < heroes.size(); j++) {
      Position h_pos = heroes[j]->getPosition();
      if (m_pos == h_pos) {
	return GameResult::HERO_LOSES;
      }
    }
  }
  return GameResult::UNKNOWN;
}

Direction BasicGameRules::getDirection(const Position &source, const Position &dest) const { //after near
  Direction d;

  if (dest == source.displace(Direction::UP)) {
    d = Direction::UP;
  } else if (dest == source.displace(Direction::DOWN)) {
    d = Direction::DOWN;
  } else if (dest == source.displace(Direction::RIGHT)) {
    d = Direction::RIGHT;
  } else if (dest == source.displace(Direction::LEFT)) {
    d = Direction::LEFT;
  }
  
  return d;
}

bool BasicGameRules::near(const Position &source, const Position &dest) const {
  return (source.distanceFrom(dest) == 1 || source.distanceFrom(dest) == 0);
}

bool BasicGameRules::inBounds(Game * game, const Position &pos) const {
  return game->getMaze()->inBounds(pos);
}


