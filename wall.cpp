//Yusra Rahman: yrahman1                                                                
//Chanha Kim: ckim135                                                                   
//Robert Aviles: raviles2 
#include "wall.h"
#include "position.h"
#include "entity.h"

MoveResult Wall::checkMoveOnto(Entity * entity, const Position &frompos, const Position &tilePos) const {
  return MoveResult::BLOCK; //no entities can move onto a wall
}

bool Wall::isGoal() const {
  return false;
}

std::string Wall::getGlyph() const {
  return "#";
}

Wall::Wall() {
}

Wall::~Wall() {
}


