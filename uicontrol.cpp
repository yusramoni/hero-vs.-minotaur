//Yusra Rahman: yrahman1                                                                
//Chanha Kim: ckim135                                                                   
//Robert Aviles: raviles2 
#include "uicontrol.h"
#include "game.h"
#include "entity.h"
#include "ui.h"

UIControl::UIControl(){
}

UIControl::~UIControl(){
}

Direction UIControl::getMoveDirection(Game * game, Entity * entity) {
  UI * ui = UI::getInstance();
  Direction dir = ui->getMoveDirection();
  return dir;
}

bool UIControl::isUser() const {
  return true; //controlled by user
}


