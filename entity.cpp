//entity.cpp
//Yusra Rahman: yrahman1                                                       
//Chanha Kim: ckim135                                                          
//Robert Aviles: raviles2 
#include <string>
#include "entity.h"
#include "entitycontroller.h"

using std::string;

Entity::Entity(){
  m_controller = NULL;
}

Entity::~Entity(){
  delete m_controller;
}

void Entity::setGlyph(const string &glyph){
  m_glyph = glyph;
}

void Entity::setProperties(const string &props){
  m_properties = props;
}

string Entity::getGlyph() const{
  return m_glyph;
}

string Entity::getProperties() const{
  return m_properties;
}

bool Entity::hasProperty(char prop) const{
  if (m_properties.find(prop) != string::npos){
    return true;
  }
  return false; //not found
}

void Entity::setController(EntityController *controller){
  delete m_controller;
  m_controller = controller;
}

EntityController * Entity::getController(){
  return m_controller;
}

void Entity::setPosition(const Position &pos){
  m_pos = pos;
}

Position Entity::getPosition() const{
  return m_pos;
}
