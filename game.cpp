//Yusra Rahman: yrahman1                                                      
//Chanha Kim: ckim135                                                          
//Robert Aviles: raviles2 
#include "game.h"
#include "maze.h"
#include "ui.h"
#include "position.h"
#include "entity.h"
#include "gamerules.h"
#include "entitycontroller.h"
#include "ecfactory.h"
#include <iostream>
#include <vector>
#include <string>
#include <sstream>

typedef std::vector<Entity *> EntityVec;


void Game::setMaze(Maze *maze) {
  this->maze = maze;
}

void Game::setUI(UI *ui) {
  this->ui = ui;
}

void Game::setGameRules(GameRules *gamerules) {
  this->gamerules = gamerules;
}

void Game::addEntity(Entity * entity) {
  entities.push_back(entity);
}

Entity * Game::getEntityAt(const Position &pos) {
  for (size_t i = 0; i < entities.size(); i++) {
    Position entityPos = entities[i]->getPosition();
    if (entityPos == pos) {
      return entities[i];
    }
  }
  return nullptr;
}

const EntityVec &Game::getEntities() const {
  return entities;
}

std::vector<Entity *> Game::getEntitiesWithProperty(char prop) const {
  std::vector<Entity *> vec;
  for (size_t i = 0; i < entities.size(); i++) {
    if (entities[i]->hasProperty(prop)) {
      vec.push_back(entities[i]);
    }
  }
  return vec;
}

Maze * Game::getMaze() {
  return this->maze;
}

UI * Game::getUI() {
  return this->ui;
}

GameRules * Game::getGameRules() {
  return this->gamerules;
}

void Game::gameLoop() {
   
  while (gamerules->checkGameResult(this) == GameResult::UNKNOWN){
    
    //go through the entities turns
    for (EntityVec::iterator it = entities.begin(); it != entities.end(); ++it){
      if (((*it)->getController())->isUser()){
	ui->render(this); //call render message
	takeTurn(*it); //enact user's move
      } else {
	takeTurn(*it); //enact Minatour's move
      }
    }
  }
 

  GameResult result = gamerules->checkGameResult(this);
  if (result == GameResult::HERO_WINS) {
    ui->displayMessage("Hero wins", true);
  } else if (result == GameResult::HERO_LOSES) {
    ui->displayMessage("Hero loses", true);
  }

  ui->render(this);
 
}

void Game::takeTurn(Entity * actor) {
  Direction d = actor->getController()->getMoveDirection(this, actor);
  Position ent_pos = actor->getPosition();
  Position hypothetical = ent_pos.displace(d);
  if (gamerules->allowMove(this, actor, ent_pos, hypothetical)){
    gamerules->enactMove(this, actor, hypothetical);
  }
  
}

Game * Game::loadGame(std::istream &in) {
  Game * game = new Game();
  Maze * maze = Maze::read(in);
  
  if (!maze) { //if maze is in a incorrect form
    return nullptr;
  }
  game->setMaze(maze);

  
  std::string firstline;
  std::string description;
  int x;
  int y;
  std::string glyph;
  char ec;
  std::string properties;
  if (!(std::getline(in, firstline))) {
    return nullptr;
  } else {
    std::stringstream ss(firstline);
    while (ss) { //read three words at a time (string, x, y)
      ss >> description;
      ss >> x;
      ss >> y;
      if(description.empty()){
	return nullptr;
      }
      if(!( x || y)) {
	return nullptr;
      } 
      glyph = description[0];
      //passed here
      if ( (glyph != "@") && (glyph != "M") && (glyph != "*") ) {
	return nullptr;
      }
      //FIXME when glyph is inanimate;
      ec = description[1];
      /*if ( (ec != 'u') || (ec != 'c') || (ec != 'a') ) {
	return nullptr;
	}*/
      properties = description.substr(2);
      Entity * entity = new Entity();
      entity->setGlyph(glyph);
      
      EntityControllerFactory * ptr = EntityControllerFactory::getInstance();
      entity->setController(ptr->createFromChar(ec));
      entity->setProperties(properties);
      entity->setPosition(Position(x,y));
 
      game->addEntity(entity);
    }
  }
  return game;
  
}

Game::Game(){
}

Game::~Game(){
  delete maze;
  delete ui;
  delete gamerules;
  for (size_t i = 0; i < (size_t)entities.size(); ++i) {
    delete entities[i];
  }
}




