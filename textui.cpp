//Yusra Rahman: yrahman1                                                                
//Chanha Kim: ckim135                                                                   
//Robert Aviles: raviles2 
#include "game.h"
#include "textui.h"
#include <string>
#include "position.h"
#include <iostream>
#include "maze.h"
#include "tile.h"
#include "entity.h"
#include <sstream>
using std::stringstream;
using std::endl; using std::cout; using std::cin;

TextUI::TextUI(){

}

TextUI::~TextUI(){

}

Direction TextUI::getMoveDirection() {
  cout << "Your move (u/d/l/r): ";
  char d;
  cin >> d;
  while (!(d == 'u' || d == 'd' || d == 'l' || d == 'r')) {
    cout << "Unknown direction" << endl;
    cout << "Your move (u/d/l/r): ";
    cin >> d;
  }
  switch(d) {
  case 'u' :
    return Direction::UP;
  case 'd' :
    return Direction::DOWN;
  case 'l' :
    return Direction::LEFT;
  case 'r' :
    return Direction::RIGHT;
  }
  return Direction::NONE;
}

void TextUI::displayMessage(const std::string &msg, bool endgame) {
  this->msg = msg;
}


void TextUI::render(Game * game) {
  //print out maze
  stringstream ss;
  Maze * maze = game->getMaze();
  int col = maze->getWidth();
  int row = maze->getHeight();
  for (int i = 0; i < row; i++) {
    for (int j = 0; j < col; j++) {
      Position pos = Position(j,i);
      Entity * entity = game->getEntityAt(pos);
      
      if (entity == nullptr) {
	ss << maze->getTile(pos)->getGlyph();
      } else {
	std::vector<Entity *> EntityVec = game->getEntities();
	std::vector<Entity *> posVec;
	for (size_t i = 0; i < EntityVec.size(); i++) {
	  if (EntityVec[i]->getPosition() == pos) {
	    posVec.push_back(EntityVec[i]);
	  }
	}
	ss << posVec[posVec.size() - 1] -> getGlyph();
      }	
    }
    cout << ss.str() << endl;
    ss.str("");
  }
  if (!msg.empty()) {
    cout << ": " <<  msg << endl;
    msg = "";
  } 
}

