#ifndef BASICGAMERULES_H
#define BASICGAMERULES_H
#include "position.h"
#include "gamerules.h"

class BasicGameRules : public GameRules {
private:
  // copy constructor and assignment operator are disallowed
  BasicGameRules(const BasicGameRules &);
  BasicGameRules &operator=(const BasicGameRules &);

public:
  BasicGameRules();
  virtual ~BasicGameRules();

  virtual bool allowMove(Game *game, Entity *actor, const Position &source, const Position &dest) const;
  virtual void enactMove(Game *game, Entity *actor, const Position &dest) const;
  virtual GameResult checkGameResult(Game *game) const;

private:
  Direction getDirection(const Position &source, const Position &dest) const;
  bool inBounds(Game * game, const Position &pos) const;
  bool near(const Position &source, const Position &dest) const;
  void display(Game * game, Entity * actor) const;
};

#endif // BASICGAMERULES_H
