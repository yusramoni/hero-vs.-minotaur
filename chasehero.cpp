//chasehero.cpp
//Yusra Rahman: yrahman1                                                       
//Chanha Kim: ckim135                                                          
//Robert Aviles: raviles2 
#include <assert.h>
#include <vector>
#include <string>
#include "chasehero.h"
#include "entity.h"
#include "position.h"
#include "game.h"
#include "gamerules.h"
#include "ecfactory.h"

using std::vector;

Direction ChaseHero::getMoveDirection(Game *game, Entity *entity){
  Position min_pos = entity->getPosition(); //get position of minatour
  int min_x = min_pos.getX();
  int min_y = min_pos.getY();
  
  vector<Entity *> hero_list = game->getEntitiesWithProperty('h'); //find all heros

  //read through vector find the closest one
  int min_distance = 100; //dummy number
  Position hero_pos;
  for (vector<Entity *>::iterator it = hero_list.begin(); it != hero_list.end(); ++it){
    Position cur_hero = (*it)->getPosition(); //get position of the hero
    int distance = min_pos.distanceFrom(hero_pos);
    if (distance < min_distance) {
      min_distance = distance;
      hero_pos = cur_hero;
    }
  }

  int hero_x = hero_pos.getX();
  int hero_y = hero_pos.getY();

  //find fastest move
  int dx = min_x - hero_x;
  int dy = min_y - hero_y;
  if (dx < 0) { dx = -dx;} //absolute value
  if (dy < 0) { dy = -dy;}

  Direction alternative;
  
  if (dx > dy){ //move in the direction of farthest distance
    if (hero_x > min_x){
      //check if move is possible
      if (checkHypothetical(Direction::RIGHT, entity, min_pos, game)){
	return Direction::RIGHT;
      } else {
	alternative = checkVertical(min_pos, entity, hero_pos, game);
	if (makeFaster(alternative, min_pos, hero_pos, dx, dy)){
	  return alternative;
	}
	return Direction::NONE;
      }
    } else { //assuming position is not equal(not caught)
      //hero_x < min_x
      if (checkHypothetical(Direction::LEFT, entity, min_pos, game)){
	return Direction::LEFT;
      } else {
	alternative = checkVertical(min_pos, entity, hero_pos, game);
	if (makeFaster(alternative, min_pos, hero_pos, dx, dy)){
	  return alternative;
	}
	return Direction::NONE;
      }
    } //end of if checking horizontal movement
  } else if (dy > dx){
    if (hero_y > min_y){
      if (checkHypothetical(Direction::DOWN, entity, min_pos, game)){
	return Direction::DOWN; 
      } else { //assuming position is not equal
	//down is blocked
	alternative = checkHorizontal(min_pos, entity, hero_pos, game);
	if (makeFaster(alternative, min_pos, hero_pos, dx, dy)){
	  return alternative;
	}
	return Direction::NONE;
      }
    } else {
      if (checkHypothetical(Direction::UP, entity, min_pos, game)){
	return Direction::UP;
      } else {
	alternative = checkHorizontal(min_pos, entity, hero_pos, game);
	if (makeFaster(alternative, min_pos, hero_pos, dx, dy)){
	  return alternative;
	}
	return Direction::NONE;
      }
    }
  } else {
    //dx = dy
    //horizontal move preferred
    if (hero_x > min_x){
      return Direction::RIGHT;
    }
    return Direction::LEFT;
  }
}

/*Check which direction to move in the vertical axis
 * In order of most desired movement
*/
Direction ChaseHero::checkVertical(Position &min_pos, Entity * entity, Position &entity_pos, Game *game){
  int hero_y = entity_pos.getY();
  int min_y = min_pos.getY();
  
  if (hero_y > min_y){
    if (checkHypothetical(Direction::DOWN, entity, min_pos, game)){
      return Direction::DOWN;
    } else if(checkHypothetical(Direction::UP, entity, min_pos, game)){
      return Direction::UP; //efficient movement not possible                                
    } else {
      //movement down, up, right not possible                                                
      return Direction::NONE; //trapped(move back??)                                         
    }
  } else {
    //hero_y < min_y                                                                         
    if (checkHypothetical(Direction::UP, entity, min_pos, game)){
      return Direction::UP;
    } else if (checkHypothetical(Direction::DOWN, entity, min_pos, game)){
      return Direction::DOWN; //efficient movement not possible                              
    } else {
      return Direction::NONE;
    }
  }
  return Direction::NONE; //assuming above will be catch all
}

Direction ChaseHero::checkHorizontal(Position &min_pos, Entity * entity, Position &entity_pos, Game* game){
  int hero_x = entity_pos.getX();
  int min_x = min_pos.getX();
  
  if (hero_x > min_x){
    if (checkHypothetical(Direction::RIGHT, entity, min_pos, game)){
      return Direction::RIGHT;
    } else if (checkHypothetical(Direction::LEFT, entity, min_pos, game)){
      return Direction::LEFT;
    } else {
      return Direction::NONE;
    }
  } else {
    if (checkHypothetical(Direction::LEFT,entity,  min_pos, game)){
      return Direction::LEFT;
    } else if (checkHypothetical(Direction::RIGHT, entity, min_pos, game)){
      return Direction::RIGHT;
    } else {
      return Direction::NONE;
    }
  }
  return Direction::NONE;
}

//If move is possible for minatour                                                                
bool ChaseHero::checkHypothetical(Direction dir, Entity * minotour, Position &min_pos, Game *game){
  Position hypothetical = min_pos.displace(dir);
  if (game->getGameRules()->allowMove(game, minotour, min_pos, hypothetical)){
    return true;
  }
  return false;;
}

bool ChaseHero::makeFaster(Direction dir, Position &min_pos, Position &hero_pos, int dx, int dy){
  Position hypothetical = min_pos.displace(dir);
  int hypo_x = hypothetical.getX();
  int hypo_y = hypothetical.getY();
  
  int hero_x = hero_pos.getX();
  int hero_y = hero_pos.getY();

  int dxnew = hypo_x - hero_x;
  int dynew = hypo_y - hero_y;
  if (dxnew < 0 ) { dxnew = -dxnew;}
  if (dynew < 0 ) { dynew = -dynew;}

  //check if the distance is reduced
  if (dxnew < dx) {
    return true;
  } else if (dynew < dy){
    return true;
  }
  return false;
}

bool ChaseHero::isUser() const{
  return false;
}

ChaseHero::ChaseHero() {
}

ChaseHero::~ChaseHero() {
}


