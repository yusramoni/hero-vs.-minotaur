//inanimate.cpp
//Yusra Rahman: yrahman1                                                       
//Chanha Kim: ckim135                                                          
//Robert Aviles: raviles2 
#include "inanimate.h"

Inanimate::Inanimate(){
}

Inanimate::~Inanimate(){
}

Direction Inanimate::getMoveDirection(Game *game, Entity *entity){
  return Direction::NONE;
}

bool Inanimate::isUser() const{
  return false; //are not controlled by user
}
