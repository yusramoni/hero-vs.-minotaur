//Yusra Rahman: yrahman1                                                       
//Chanha Kim: ckim135                                                          
//Robert Aviles: raviles2 
#include "goal.h"
#include "position.h"
#include "entity.h"

MoveResult Goal::checkMoveOnto(Entity *entity, const Position &frompos, const Position &tilepos) const {
  /*if (!entity->hasProperty('v')) { //if entity is not moveable
    return MoveResult::BLOCK;
  }
  int gx = tilepos.getX(); //get goal's x
  int gy = tilepos.getY(); //get goal's y
  int fx = frompos.getX(); //get from's x
  int fy = frompos.getY(); //get from's y
  int dx = gx - fx; //x distance
  int dy = gy - fy; //y distance
  if ((dx == 1) ^ (dy == 0)) { 
    return MoveResult::ALLOW;
  } else if ((dx == -1) ^ (dy == 0)) {
    return MoveResult::ALLOW;
  } else if ((dx == 0) ^ (dy == 1)) {
    return MoveResult::ALLOW;
  } else if ((dx == 0) ^ (dy == -1)) {
    return MoveResult::ALLOW;
  } else {
    return MoveResult::BLOCK;
  }
  return MoveResult::BLOCK;
  */
  return MoveResult::ALLOW;
}

bool Goal::isGoal() const {
  return true;
}

std::string Goal::getGlyph() const {
  return "<";
}

Goal::Goal(){
}

Goal::~Goal() {
}



